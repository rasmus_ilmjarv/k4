/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      System.out.println(valueOf("4/8"));
      System.out.println(valueOf("9/3"));
      System.out.println(valueOf("2/8").equals(valueOf("1/4")));
      System.out.println(valueOf("2/8").hashCode() == valueOf("1/4").hashCode());

      System.out.println(findSmallestContainingBoth(5, 15));

      System.out.println(valueOf("-4/75").inverse());

      System.out.println(valueOf("2/5").minus(valueOf("4/15")));

      System.out.println(toLfraction (Math.PI, 7));
      System.out.println(valueOf("2/5").hashCode() == valueOf("2/5").hashCode());
      System.out.println(valueOf("5/2").hashCode() == valueOf("2/5").hashCode());
      System.out.println(valueOf("0/2").hashCode() == valueOf("0/1").hashCode());
   }

   private long numerator;
   private long denominator;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      if (b == 0) {
         throw new ArithmeticException("Fraction denominator can't be zero: " + b);
      }
      if (a != 0L) {
         long divider = findGreatestCommonFactor(a, b);
         a /= divider;
         b /= divider;
      }
      if (b < 0) {
         b = -b;
         a = -a;
      }
      if (a == 0L) b = 1;
      numerator = a;
      denominator = b;
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return numerator;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return numerator + "/" + denominator;
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      if (!(m instanceof Lfraction)) {
         return false;
      }
      Lfraction other = (Lfraction) m;
      return this.compareTo(other) == 0;
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Long.hashCode(numerator) + Long.hashCode(denominator + numerator);
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      long newDenominator = findSmallestContainingBoth(denominator, m.denominator);
      return new Lfraction(numerator * (newDenominator / denominator) + m.numerator * (newDenominator / m.denominator),
              newDenominator);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      return new Lfraction(m.numerator * numerator, m.denominator * denominator);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if (numerator == 0) {
         throw new RuntimeException(" Division by zero not allowed: " + denominator + "/" + numerator);
      }
      return new Lfraction(denominator, numerator);
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(-numerator, denominator);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      return this.plus(m.opposite());
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      return this.times(m.inverse());
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
       long newDenominator = findSmallestContainingBoth(denominator, m.denominator);
       long a = numerator * (newDenominator / denominator);
       long b = m.numerator * (newDenominator / m.denominator);
       if (a < b) {
          return -1;
       }
       return b < a ? 1: 0;
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(numerator, denominator);
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return numerator / denominator;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      return new Lfraction(numerator % denominator, denominator);
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      double l = (double) numerator / (double) denominator;
      return l;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      Lfraction fraction = new Lfraction(1, d);
      fraction.numerator = Math.round(f * d);
      return fraction;
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      RuntimeException exception = new RuntimeException(" input is not a fraction, it must be a <numerator>/<denominator>: " + s);
      if (s.length() < 3 || !s.contains("/")){
         throw exception;
         /*
         if (!isLong(s)) throw exception;
         else return new Lfraction(Long.parseLong(s), 1);
          */
      }
      long[] value = new long[2];
      String[] toTranslate = s.split("/");
      if (isLong(toTranslate[0])) {
         value[0] = Long.parseLong(toTranslate[0]);
         if (isLong(toTranslate[1])) {
            value[1] = Long.parseLong(toTranslate[1]);
         } else {
            throw exception;
         }
      } else {
         throw exception;
      }
      return new Lfraction(value[0], value[1]);
   }

   /**
    * Raise the fraction to the power of n.
    *
    * @param n power
    * @return this fraction to the power of n: this ^ n
    */
   public Lfraction pow(long n) {
      if (n == 1) return new Lfraction(numerator, denominator);
      if (n == 0) return new Lfraction(1, 1);
      if (n < 0) return pow(Math.abs(n)).inverse();

      return times(pow(n - 1));
   }

   private static boolean isLong(String str) {
      try {
         Long.parseLong(str);
         return true;
      } catch (NumberFormatException e) {
         return false;
      }
   }

   private static long findGreatestCommonFactor(long a, long b) {
      long greatest = 1;
      for (int i = 1; a >= i && b >= i; i++) {
         if (a % i == 0 && b % i == 0) {
            greatest = i;
         }
      }
      return greatest;
   }


   private static long findSmallestContainingBoth(long a, long b) {
      long result = a * b;
      for (long i = a > b ? a: b; i < a * b; i++) {
         if (i % a == 0 && i % b == 0) {
            result = i;
            break;
         }
      }
      return result;
   }
}

